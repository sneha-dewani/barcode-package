{{--getBarcode...('text','code',[width,height,color,showText])--}}
{{--all the codes for 1D barcode and 2D barcode are mentioned in the website--}}
{{--Also enable PHP-gd extension if it is not enabled--}}

<?php
//1D Barcode in Svg
echo DNS1D::getBarcodeSVG('4445645656', 'PHARMA2T', 2, 30, 'red', true);

//1d Barcode in HTML
echo DNS1D::getBarcodeHTML('4445645656', 'PHARMA2T');

//2D barcode in HTML
echo DNS2D::getBarcodeHTML('4445645656', 'QRCODE');

//2D barcode in SVG
echo DNS2D::getBarcodeSVG('4445645656', 'DATAMATRIX');
?>

<html>
<body>
<br>
{{--1d barcode in PNG--}}
<img src="data:image/png;base64,{{ DNS1D::getBarcodePNG("4", "C128B")}}" alt="barcode" />

<br>
<br>
{{--1d barcode in JPG--}}
<img src="data:image/png;base64,{{ DNS1D::getBarcodeJPG("4", "C39+")}}" alt="barcode" />
<br>
<br>
{{--for png and jpg we have to specify rgb code instead of the name of the color--}}
<img src="data:image/png;base64,{{ DNS1D::getBarcodePNG('4353', 'C39+',3,33,array(175,54,60),true)}}" alt="barcode" />

<br>
<br>
{{--REMEMBER:for 2d barcode, there is not getBarcodeJPG()--}}
<img src="data:image/png;base64,{{ DNS2D::getBarcodePNG("4", "DATAMATRIX",3,10,array(70,148,73))}}" alt="barcode" />
</body>
</html>




